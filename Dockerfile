FROM maven:3.5.2-jdk-8-alpine

ADD . /src
WORKDIR /src

RUN mvn package \
    && mv target/*.jar app.jar \
    && mvn clean

ENV REDIS_HOST=redis

CMD java -jar app.jar --spring.redis.host=$REDIS_HOST