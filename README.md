
To run application this app clone this repo, go into the repo folder and run:
```bash
docker-compose up --build
```

Test, for given name you should get second response from cache 
```bash
> curl localhost:8080?name=Neo
Hello Neo. Your lucky number is 3. I found it in 3005 millis
> curl localhost:8080?name=Neo
Hello Neo. Your lucky number is 3. I found it in 1 millis

```

If you want to play with app and change sth, stop if from docker compose, build and run. Make sure that redis is running
```bash

docker-compose stop app
docker-compose up -d redis

mvn package 
java -jar target/redis-cache-0.0.1-SNAPSHOT.jar
```

If you have your own redis server you can pass host name to app
```bash
set REDIS_HOST=your.host
mvn package 
java -jar target/redis-cache-0.0.1-SNAPSHOT.jar --spring.redis.host=$REDIS_HOST
```