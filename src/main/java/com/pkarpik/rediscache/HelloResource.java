package com.pkarpik.rediscache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResource {

    @Autowired
    LuckyNumberService luckyNumberService;

    @RequestMapping("/")
    public String greeting(@RequestParam(value = "name", defaultValue = "Stranger") String name) {
        long start = System.currentTimeMillis();
        Integer luckyNumber = luckyNumberService.findLuckyNumber(name);
        long end = System.currentTimeMillis();
        return String.format("Hello %s. Your lucky number is %d. I found it in %s millis", name, luckyNumber, end-start);
    }


}
