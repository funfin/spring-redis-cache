package com.pkarpik.rediscache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class LuckyNumberService {

    @Cacheable("lucky-number")
    public Integer findLuckyNumber(String name) {
        simulateSlowService();
        return name.length();
    }

    // Don't do this at home
    private void simulateSlowService() {
        try {
            long time = 3000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
